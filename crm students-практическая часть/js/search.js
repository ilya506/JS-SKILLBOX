import { createTableBody } from "./table.js";

export default async function autocomplete(
  inputElement,
  primaryArray,
  secondaryArray
) {
  let currentFocus = -1;

  inputElement.addEventListener("input", function () {
    const value = this.value.trim();
    if (!value) {
      closeAllLists();
      return;
    }

    closeAllLists();
    currentFocus = -1;

    const autocompleteList = createAutocompleteList(this);

    [primaryArray, secondaryArray].forEach((array) => {
      populateAutocompleteList(array, value, autocompleteList, inputElement);
    });
  });

  inputElement.addEventListener("keydown", function (e) {
    let items = document.getElementById(`${this.id}-autocomplete-list`);
    if (items) items = items.getElementsByTagName("div");

    switch (e.keyCode) {
      case 40:
        currentFocus++;
        highlightItem(items);
        break;
      case 38:
        currentFocus--;
        highlightItem(items);
        break;
      case 13:
        e.preventDefault();
        if (currentFocus > -1 && items) items[currentFocus].click();
        break;
    }
  });

  function createAutocompleteList(input) {
    const list = document.createElement("div");
    list.setAttribute("id", `${input.id}-autocomplete-list`);
    list.classList.add("header__search-list", "autocomplete-items");
    input.parentNode.appendChild(list);
    return list;
  }

  function populateAutocompleteList(array, value, list, input) {
    array.forEach((item) => {
      if (item.toLowerCase().startsWith(value.toLowerCase())) {
        const listItem = document.createElement("div");
        listItem.classList.add("header__search-item", "autocomplete-item");

        listItem.innerHTML = `
          <span class="mark">${item.substr(
            0,
            value.length
          )}</span>${item.substr(value.length)}
          <input type="hidden" value="${item}">
        `;

        listItem.addEventListener("click", () => {
          input.value = item;
          closeAllLists();
          createTableBody({ filter: item });
        });

        list.appendChild(listItem);
      }
    });
  }

  function highlightItem(items) {
    if (!items) return;

    removeHighlight(items);
    if (currentFocus >= items.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = items.length - 1;
    items[currentFocus].classList.add("autocomplete-active");
  }

  function removeHighlight(items) {
    Array.from(items).forEach((item) =>
      item.classList.remove("autocomplete-active")
    );
  }

  function closeAllLists(exceptElement) {
    const lists = document.querySelectorAll(".autocomplete-items");
    lists.forEach((list) => {
      if (list !== exceptElement && list !== inputElement) list.remove();
    });
  }

  document.addEventListener("click", (e) => closeAllLists(e.target));
}
