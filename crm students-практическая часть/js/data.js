const BASE_URL = "http://localhost:3000/api/clients";

const fetchData = async (url, options = {}) => {
  const response = await fetch(url, options);
  const data = await response.json();
  return { data, status: response.status };
};

export const getUsersFromServer = async (query = "") => {
  const { data, status } = await fetchData(`${BASE_URL}?search=${query}`);
  return { users: data, status };
};

export const getUserToIdFromServer = async (id) => {
  const { data } = await fetchData(`${BASE_URL}/${id}`);
  return data;
};

export const addUserToServer = async (user) => {
  const { data } = await fetchData(BASE_URL, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(user),
  });
  return data;
};

export const updateUserToServer = async (id, user) => {
  const { data } = await fetchData(`${BASE_URL}/${id}`, {
    method: "PATCH",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(user),
  });
  return data;
};

export const deleteUserToServer = async (id) => {
  const { data } = await fetchData(`${BASE_URL}/${id}`, {
    method: "DELETE",
  });
  return data;
};
