export default async function sortUsers(users, sortType = "id", direction = 1) {
  const SORT_TYPE = {
    id: "id",
    fullname: "fullname",
    createdAt: "createdAt",
    updatedAt: "updatedAt",
  };

  const compareValues = (a, b, flag) => {
    if (a > b) return flag;
    if (a < b) return -flag;
    return 0;
  };

  const getFullName = (user) => `${user.surname} ${user.name} ${user.lastName}`;

  const sortStrategies = {
    [SORT_TYPE.id]: (a, b) => compareValues(a.id, b.id, direction),
    [SORT_TYPE.fullname]: (a, b) =>
      compareValues(getFullName(a), getFullName(b), direction),
    [SORT_TYPE.createdAt]: (a, b) =>
      compareValues(a.createdAt, b.createdAt, direction),
    [SORT_TYPE.updatedAt]: (a, b) =>
      compareValues(a.updatedAt, b.updatedAt, direction),
  };

  const sortFunction = sortStrategies[sortType];

  return sortFunction ? users.sort(sortFunction) : users;
}
