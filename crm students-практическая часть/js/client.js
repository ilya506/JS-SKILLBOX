import { getUserToIdFromServer } from "./data.js";

document.addEventListener("DOMContentLoaded", async () => {
  const wrapper = document.querySelector(".client__wrapper");
  const main = document.querySelector(".client__main");
  const container = createElementWithClasses("div", [
    "container",
    "client__container",
  ]);
  const clientInfo = createElementWithClasses("div", ["client__info"]);
  const clientPhoto = createElementWithClasses("div", ["client__photo"]);
  main.classList.add("main");

  const id = new URLSearchParams(window.location.search).get("id");
  const user = await getUserToIdFromServer(id);

  function createElementWithClasses(tag, classes = []) {
    const element = document.createElement(tag);
    classes.forEach((cls) => element.classList.add(cls));
    return element;
  }

  function createClientHeader(parent) {
    const header = createElementWithClasses("div", ["header"]);
    const headerContainer = createElementWithClasses("div", [
      "container",
      "header__container",
    ]);
    const logo = createElementWithClasses("div", ["header__logo"]);
    const headerTitle = createElementWithClasses("h2", [
      "client__header-title",
    ]);

    headerTitle.innerText = "Карточка клиента";

    headerContainer.append(logo, headerTitle);
    header.append(headerContainer);
    parent.append(header);
  }

  function createClientTitle() {
    const titleContainer = createElementWithClasses("div", [
      "client__title-container",
    ]);
    const title = createElementWithClasses("h1", ["client__title"]);
    const clientID = createElementWithClasses("span", ["client__id"]);

    title.innerHTML = `${user.surname} ${user.name} ${user.lastName}`;
    clientID.innerHTML = `id: ${id}`;

    titleContainer.append(title, clientID);
    clientInfo.append(titleContainer);
  }

  function formatDate(date) {
    const pad = (num) => String(num).padStart(2, "0");
    const day = pad(date.getDate());
    const month = pad(date.getMonth() + 1);
    const year = date.getFullYear();
    const time = `${pad(date.getHours())}:${pad(date.getMinutes())}`;
    return `${day}.${month}.${year}г. <span class="client__data-time">${time}</span>`;
  }

  function createDateElement(labelText, date) {
    const container = createElementWithClasses("div", ["client__data-date"]);
    const label = createElementWithClasses("span", ["client__data-label"]);
    const dateElement = createElementWithClasses("span", ["client__data-date"]);

    label.innerText = labelText;
    dateElement.innerHTML = formatDate(date);

    container.append(label, dateElement);
    return container;
  }

  function createClientInfo() {
    const info = createElementWithClasses("div", ["client__data"]);
    const dateCreate = new Date(user.createdAt);
    const dateChange = new Date(user.updatedAt);

    info.append(
      createDateElement("Дата и время создания: ", dateCreate),
      createDateElement("Последние изменения: ", dateChange)
    );
    clientInfo.append(info);
  }

  function createClientContacts() {
    const contactsContainer = createElementWithClasses("div", [
      "client__contacts",
    ]);
    const contactIcons = {
      Телефон: "img/phone.svg",
      Email: "img/mail.svg",
      Vk: "img/vk.svg",
      Facebook: "img/facebook.svg",
      Другое: "img/other.svg",
    };

    user.contacts.forEach((contact) => {
      const contactContainer = createElementWithClasses("div", [
        "client__contacts-container",
      ]);
      const contactIcon = createElementWithClasses("img", [
        "client__contacts-item",
      ]);
      const contactDescr = createElementWithClasses("span", [
        "client__contacts-descr",
      ]);
      const link = document.createElement("a");

      contactIcon.src = contactIcons[contact.type] || contactIcons["Другое"];
      link.href =
        contact.type === "Телефон"
          ? `tel:${contact.value}`
          : contact.type === "Email"
          ? `mailto:${contact.value}`
          : `http://${contact.value}`;
      link.target = "_blank";
      link.textContent = contact.value;

      contactDescr.innerHTML = `${contact.type}: `;
      contactDescr.append(link);

      contactContainer.append(contactIcon, contactDescr);
      contactsContainer.append(contactContainer);
    });

    clientInfo.append(contactsContainer);
  }

  function createClientPhoto(parent) {
    const clientAvatar = createElementWithClasses("div", ["client__avatar"]);
    parent.append(clientAvatar);
  }

  function createBackButton(parent) {
    const btnContainer = createElementWithClasses("div", [
      "client__btn-container",
    ]);
    const backButton = createElementWithClasses("a", ["client__btn"]);

    backButton.href = "javascript:history.back()";
    backButton.textContent = "Назад";

    btnContainer.append(backButton);
    parent.append(btnContainer);
  }

  createClientHeader(wrapper);
  createClientTitle();
  createClientInfo();
  createClientContacts();
  createClientPhoto(clientPhoto);
  container.append(clientInfo, clientPhoto);
  main.append(container);
  createBackButton(main);
  wrapper.append(main);
});
